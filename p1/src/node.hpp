/**
* Clase nodo que será utilizada por las clases lista, pila y cola
*/

#pragma once

#include <iostream>

using namespace std;

typedef int TDATO;

class Node {
    private:
        TDATO value_;
        Node *next_;

    public:
        Node();
        Node(TDATO&);
        ~Node();

        void set_value(TDATO&);
        void set_next(Node*);
        TDATO get_value();
        Node* get_next();
};
