/**
* Clase cola
*/

#pragma once

#include "list.hpp"

class Queue {
    private:
        List *queue_;

    public:
        Queue();
        ~Queue();

        void push(TDATO&);
        TDATO pop();

    friend ostream& operator << (ostream&, const Queue&);
};
