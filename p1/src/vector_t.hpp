/**
* Clase vector
*/

#pragma once
#include <iostream>

using namespace std;

typedef int TDATO;

class vector_t {

    private:
        TDATO* base_;
        int sz_;

    public:
        vector_t();
        vector_t(int sz);
        virtual ~vector_t();

        void resize(int sz);
        TDATO& operator[](int pos);
        TDATO operator[](int pos) const;
        int get_size();        

    private:
        void build(int sz);
        void clean();

    friend ostream& operator<<(ostream& os,const vector_t& v);

};
