#include "node.hpp"

Node::Node() {
    value_ = 0;
    next_ = NULL;
}

Node::Node(TDATO& n) {
    value_ = n;
    next_ = NULL;
}

Node::~Node() { }

void Node::set_value(TDATO& value) {
  value_ = value;
}

void Node::set_next(Node* next) {
  next_ = next;
}

TDATO Node::get_value() {
  return value_;
}

Node* Node::get_next() {
  return next_;
}
