#include "stack.hpp"

Stack::Stack() {
  stack_ = new List();
}

Stack::Stack(const Stack& s) {
    *this = s;
}

Stack::~Stack() { }

void Stack::push(TDATO n) {
  stack_->push_front(n);
}

TDATO Stack::pop() {
  return stack_->pop();
}

ostream& operator << (ostream& os, const Stack& stack) {
  os << *stack.stack_;
  return os;
}
