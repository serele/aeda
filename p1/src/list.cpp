#include "list.hpp"

List::List() {
    first_ = NULL;
    last_ = NULL;
}

List::List(const List& l) {
    *this = l;
}

List::~List() {
    Node *current;
    if (first_ == NULL)
        return;

    while(first_ != NULL) {
        current = first_->get_next();
        delete first_;
        first_ = current;
    }
}


void List::push_back(TDATO n) {
    Node *aux;

    // Primera vez que se inserta un nodo
    if(first_ == NULL) {
        first_ = new Node;
        first_->set_value(n);
        first_->set_next(NULL);
        last_ = first_;
    }
    else {
        aux = new Node;
        aux->set_value(n);
        aux->set_next(NULL);
        last_->set_next(aux);
        last_ = aux;
    }
}

void List::push_front(TDATO n) {
    Node *aux;
    aux = new Node(n);

    if(first_ != NULL) {
        aux->set_next(first_);
    }
    else {
        last_ = aux;
    }
    first_ = aux;
}

TDATO List::pop() {
    Node *aux;
    TDATO value;
    if (first_ != NULL ){
        aux = first_->get_next();
        value = first_->get_value();
        delete first_;
        first_ = aux;
    }
    return value;
}

void List::erase(TDATO n) {
    Node *current, *aux;
    current = first_;

    if(current->get_value() == n) {
        first_ = current->get_next();
        delete current;
        return;
    }

    aux = current;

    while(current != NULL) {
        if(current->get_value() == n) {
            aux->set_next(current->get_next());
            delete current;
            return;
        }
        aux = current;
        current = current->get_next();
    }
    cout << endl << "Elemento " << n << " no encontrado" << endl;
}

int List::get_size() {
    Node *current;
    int n = 0;

    for(current = first_; current != NULL; current = current->get_next()) {
        n++;
    }

    return n;
}


ostream& operator<< (ostream& os, const List& list) {
    Node *current;
    os << endl;

    for(current = list.first_; current != NULL; current = current->get_next())
        os << current->get_value() << "\t";

    os << endl;
}
