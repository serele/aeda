/*
SERGIO REYES DE LEÓN
Algoritmos y Estructuras de Datos Avanzadas

Práctica 1. Implementación de estructuras de datos

*/

#include "vector_t.hpp"
#include "list.hpp"
#include "stack.hpp"
#include "queue.hpp"

void pruebas_vector() {
    vector_t A(8);
    vector_t B;
    B.resize(5);

    cout << "Tamaño de A: " << A.get_size() << endl;
    cout << "Tamaño de B: " << B.get_size() << endl;

    for(int i = 0; i < A.get_size(); i++) {
        A[i]= i;
    }

    cout << "A = " << A << endl;
    cout << "B = " << B << endl;
    cout << endl;
}

void pruebas_lista() {
    int n = 0;
    cout << "Nº de elementos que contendrá la lista: ";
    cin >> n;
    List lista;
    for(int i = 0; i < n; i++) {
        lista.push_back(i);
    }

    cout << "Lista de " << n << " elementos: " << lista << endl;

    cout << "Nº de elementos a eliminar de la lista: ";
    cin >> n;
    for(int i = 0; i < n; i++) {
        lista.pop();
    }

    cout << lista << endl;

    TDATO tdato;
    cout << "Insertar elemento al principio de la lista: ";
    cin >> tdato;
    lista.push_front(tdato);
    cout << lista << endl;
    cout << lista.get_size() << endl;
    cout << endl;
}

void pruebas_pila() {
    int m = 0;
    cout << "Nº de elementos que contendrá la pila: ";
    cin >> m;
    Stack pila;
    for(int i = 0; i < m; i++) {
        pila.push(i);
    }

    cout << "Pila de " << m << " elementos: " << pila << endl;

    cout << "Nº de elementos a eliminar de la pila: ";
    cin >> m;
    for(int i = 0; i < m; i++) {
        pila.pop();
    }

    cout << pila << endl;
    cout << endl;
}

void pruebas_cola() {
    int k = 0;
    cout << "Nº de elementos que contendrá la cola: ";
    cin >> k;
    Queue cola;
    for(int i = 0; i < k; i++) {
        cola.push(i);
    }

    cout << "Cola de " << k << " elementos: " << cola << endl;

    cout << "Nº de elementos a eliminar de la cola: ";
    cin >> k;
    for(int i = 0; i < k; i++) {
        cola.pop();
    }

    cout << cola << endl;
    cout << endl;

}

int main() {
    cout << endl;

    // pruebas_vector();
    pruebas_lista();
    // pruebas_pila();
    // pruebas_cola();

    return 0;
}
