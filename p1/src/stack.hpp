/**
* Clase pila
*/

#pragma once

#include "list.hpp"

class Stack {
    private:
        List *stack_;

    public:
        Stack();
        Stack(const Stack&);
        ~Stack();

        void push(TDATO);
        TDATO pop();

    friend ostream& operator << (ostream&, const Stack&);
};
