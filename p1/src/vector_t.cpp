#include "vector_t.hpp"

vector_t::vector_t() : base_(NULL), sz_(0) {}

vector_t::vector_t(int sz) : base_(NULL), sz_(0) {
    build(sz);
}

vector_t::~vector_t() {
    clean();
}

void vector_t::build(int sz) {
    if(base_ == NULL) {
        base_ = new TDATO[sz];
        sz_ = sz;
    }
    else {
        cerr << "ERROR" << endl;
    }
}

void vector_t::clean() {
    if(base_ != NULL) {
        delete [] base_;
        base_ = NULL;
        sz_ = 0;
    }
}

void vector_t::resize(int sz) {
    clean();
    if(sz > 0) build(sz);
}

TDATO& vector_t::operator[](int pos) {
    return base_[pos];
}

TDATO vector_t::operator[](int pos) const {
    return base_[pos];
}

int vector_t::get_size() {
    return sz_;
}

ostream& operator << (ostream& o, const vector_t& v) {
    o << "(";

    for(int i = 0; i < v.sz_; i++) {
        o << v[i];
        if (i < v.sz_-1)
          o << ", ";
    }

    o << ")";;
    return o;
}
