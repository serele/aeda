#include "queue.hpp"

Queue::Queue() {
    queue_ = new List();
}

Queue::~Queue() {}

void Queue::push(TDATO& n) {
    queue_->push_back(n);
}

TDATO Queue::pop() {
    return queue_->pop();
}

ostream& operator << (ostream& os, const Queue& q) {
    os << *q.queue_;
    return os;
}
