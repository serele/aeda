/*
 * List.h
 *
 *  Created on: Mar 8, 2016
 *      Author: sergio
 */

#ifndef LIST_H_
#define LIST_H_

#include "Node.h"
#include <iostream>

using namespace std;

template <class T>

class List {

private:
  Node<T> *first, *last;

public:
  List();
  List(const List&);
  ~List();

  Node<T> getFirst() const;
  Node<T> getLast() const;
  void push_back(T);
  void push_front(T);
  T pop();
  bool erase(T);

  friend ostream& operator << (std::ostream& o, const List<T>& list) {
    Node<T> *current;

    for(current = list.first; current != NULL; current = current->getNext())
      o << current->getValue() << " ";


    return o;
  }

};

class ExcepcionErrorMemoriaList : public exception {
public:
    const char* what() const throw() {
	return "Error: fallo en la reserva de memoria dinámica...";
    }
};


template <class T>
List<T>::List() {
  first = NULL;
  last = NULL;
}

template <class T>
List<T>::List(const List& l) {
  *this = l;
}

template <class T>
List<T>::~List() {
  Node<T> *current;
  if (first == NULL)
    return;

  while(first != NULL) {
    current = first->getNext();
    delete first;
    first = current;
  }
}

template <class T>
Node<T> List<T>::getFirst() const{
  return this->first;
}

template <class T>
Node<T> List<T>::getLast() const{
  return this->last;
}

template <class T>
void List<T>::push_back(T n) {
  Node<T> *aux;

  // Primera vez que se inserta un nodo
  if(first == NULL) {
    try {
      first = new Node<T>;
    }
    catch(std::bad_alloc& e) {
      throw ExcepcionErrorMemoriaList();
    }

    first->setValue(n);
    first->setNext(NULL);
    last = first;
  }
  else {
    try {
      aux = new Node<T>;
    }
    catch(std::bad_alloc& e) {
      throw ExcepcionErrorMemoriaList();
    }

    aux->setValue(n);
    aux->setNext(NULL);
    last->setNext(aux);
    last = aux;
  }
}

template <class T>
void List<T>::push_front(T n) {
  Node<T> *aux;

  try {
    aux = new Node<T>(n);
  }
  catch(std::bad_alloc& e) {
    throw ExcepcionErrorMemoriaList();
  }

  if(first != NULL) {
    aux->setNext(first);
  }
  else {
    last = aux;
  }
  first = aux;
}

template <class T>
T List<T>::pop() {
  Node<T> *aux;
  T value;
  if (first != NULL ){
    aux = first->getNext();
    value = first->getValue();
    delete first;
    first = aux;
  }
  return value;
}

template <class T>
bool List<T>::erase(T n) {
  Node<T> *current, *aux;
  current = first;

  if(current->getValue() == n) {
    first = current->getNext();
    delete current;
    return true;
  }

  aux = current;

  while(current != NULL) {
    if(current.getValue() == n) {
      aux->setNext(current->getNext());
      delete current;
      return true;
    }
    aux = current;
    current = current->getNext();
  }
  return false;
}

