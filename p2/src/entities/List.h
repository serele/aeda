/*
 * List.h
 *
 *  Created on: Mar 8, 2016
 *      Author: sergio
 */

#ifndef LIST_H_
#define LIST_H_

#include "Node.h"
#include <iostream>

using namespace std;

template <class T>

class List {

private:
  Node<T> *first_, *last_;

public:
  List();
  List(const List&);
  ~List();

  Node<T> getFirst() const;
  Node<T> getLast() const;
  void push_back(T);
  void push_front(T);
  T pop();
  bool erase(T);

  friend ostream& operator << (ostream& os, const List<T>& list) {
    Node<T> *current;

    for(current = list.first_; current != NULL; current = current->getNext())
      os << current->getValue() << " ";


    return os;
  }
};

class ExcepcionErrorMemoriaList : public exception {
public:
    const char* what() const throw() {
	return "Error: fallo en la reserva de memoria dinámica...";
    }
};


template <class T>
List<T>::List() {
  first_ = NULL;
  last_ = NULL;
}

template <class T>
List<T>::List(const List& l) {
  *this = l;
}

template <class T>
List<T>::~List() {
  Node<T> *current;
  if (first_ == NULL)
    return;

  while(first_ != NULL) {
    current = first_->getNext();
    delete first_;
    first_ = current;
  }
}

template <class T>
Node<T> List<T>::getFirst() const{
  return this->first_;
}

template <class T>
Node<T> List<T>::getLast() const{
  return this->last_;
}

template <class T>
void List<T>::push_back(T n) {
  Node<T> *aux;

  // Primera vez que se inserta un nodo
  if(first_ == NULL) {
    try {
      first_ = new Node<T>;
    }
    catch(std::bad_alloc& e) {
      throw ExcepcionErrorMemoriaList();
    }

    first_->setValue(n);
    first_->setNext(NULL);
    last_ = first_;
  }
  else {
    try {
      aux = new Node<T>;
    }
    catch(std::bad_alloc& e) {
      throw ExcepcionErrorMemoriaList();
    }

    aux->setValue(n);
    aux->setNext(NULL);
    last_->setNext(aux);
    last_ = aux;
  }
}

template <class T>
void List<T>::push_front(T n) {
  Node<T> *aux;

  try {
    aux = new Node<T>(n);
  }
  catch(std::bad_alloc& e) {
    throw ExcepcionErrorMemoriaList();
  }

  if(first_ != NULL) {
    aux->setNext(first_);
  }
  else {
    last_ = aux;
  }
  first_ = aux;
}

template <class T>
T List<T>::pop() {
  Node<T> *aux;
  T value;
  if (first_ != NULL ){
    aux = first_->getNext();
    value = first_->getValue();
    delete first_;
    first_ = aux;
  }
  return value;
}

template <class T>
bool List<T>::erase(T n) {
  Node<T> *current, *aux;
  current = first_;

  if(current->getValue() == n) {
    first_ = current->getNext();
    delete current;
    return true;
  }

  aux = current;

  while(current != NULL) {
    if(current->getValue() == n) {
      aux->setNext(current->getNext());
      delete current;
      return true;
    }
    aux = current;
    current = current->getNext();
  }
  return false;
}

#endif /* LIST_H_ */
