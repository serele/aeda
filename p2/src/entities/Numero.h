/*
 * Numero.h
 *
 *  Created on: Mar 9, 2016
 *      Author: sergio
 */

#ifndef NUMERO_H_
#define NUMERO_H_

#include <cmath>
#include "Excepciones.h"

using namespace std;

class Numero {

    public:
        virtual ostream& toStream (ostream& sout) const = 0;
        virtual istream& fromStream (istream& sin) const = 0;
};



#endif /* NUMERO_H_ */
