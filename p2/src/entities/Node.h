/*
 * Node.h
 *
 *  Created on: Mar 8, 2016
 *      Author: sergio
 */

#ifndef NODE_H_
#define NODE_H_

#include <iostream>

using namespace std;

template <class T>

class Node {

private:
  T value_;
  Node *next_;

public:
  Node();
  Node(T);
  Node(const Node&);
  ~Node();

  void setValue(T);
  void setNext(Node<T>*);
  T getValue() const;
  Node<T>* getNext();
};


template <class T>
Node<T>::Node() {
  value_ = T();
  next_ = NULL;
}

template <class T>
Node<T>::Node(T n) {
  value_ = n;
  next_ = NULL;
}

template <class T>
Node<T>::Node(const Node &n) {
  value_ = n.value_;
  next_ = n.next_;
}

template <class T>
Node<T>::~Node() { }

template <class T>
void Node<T>::setValue(T n) {
  value_ = n;
}

template <class T>
void Node<T>::setNext(Node<T>* n) {
  next_ = n;
}

template <class T>
T Node<T>::getValue() const{
  return value_;
}

template <class T>
Node<T>* Node<T>::getNext() {
  return next_;
}

#endif /* NODE_H_ */
