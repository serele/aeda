/*
 * Calculadora.h
 *
 *  Created on: Mar 9, 2016
 *      Author: sergio
 */

#ifndef ENTITIES_CALCULADORA_H_
#define ENTITIES_CALCULADORA_H_

#include "../vo/Stack.h"

template <class T>

class Calculadora {

private:
	Stack<T> pila_;
public:
	Calculadora();
	virtual ~Calculadora();

	void push(T);
	T pop();

	void suma();
	void resta();
	void multiplicacion();
	void division();
//	void potencia();
	void sqrt();

	Stack<T>& get_stack();

//	friend istream& operator >> (istream &is, Calculadora &c) {
//
//	    while (is.peek() != '\n') {
//	    	char str;
//			is >> ws;  // eat up any leading white spaces
//			T c = is.peek();  // peek character
//			if ( isdigit(c) || (str == '-' && isdigit(cin.peek())) ) {
//				T n;
//				cin >> n;
//				c.pila_.push(n);
//				cout << "Operando: " << n << '\n';
//				cout << "Contenido de la pila: " >> c.pila_;
//			}
//	    }
//
//	    return is;
//	}
};

template <class T>
Calculadora<T>::Calculadora() { }

template <class T>
Calculadora<T>::~Calculadora() { }

template <class T>
void Calculadora<T>::push(T n) {
  pila_.push(n);
}

template <class T>
T Calculadora<T>::pop() {
  return pila_.pop();
}

template <class T>
void Calculadora<T>::suma() {
	T n1, n2;
	n2 = pila_.pop();
	n1 = pila_.pop();
	pila_.push(n1 + n2);
	cout << n1 << " + " << n2 << " = " << n1 + n2 << endl;
}

template <class T>
void Calculadora<T>::resta() {
	T n1, n2;
	n2 = pila_.pop();
	n1 = pila_.pop();
	pila_.push(n1 - n2);
	cout << n1 << " - " << n2 << " = " << n1 - n2 << endl;
}

template <class T>
void Calculadora<T>::multiplicacion() {
	T n1, n2;
	n2 = pila_.pop();
	n1 = pila_.pop();
	pila_.push(n1 * n2);
	cout << n1 << " * " << n2 << " = " << n1 * n2 << endl;
}

template <class T>
void Calculadora<T>::division() {
	T n1, n2;
	n2 = pila_.pop();
	n1 = pila_.pop();
	pila_.push(n1 / n2);
	cout << n1 << " / " << n2 << " = " << n1 / n2 << endl;
}

//template <class T>
//void Calculadora<T>::potencia() {
//	T n1, n2;
//	n2 = pila_.pop();
//	n1 = pila_.pop();
//	pila_.push(n1 ^ n2);
//	cout << n1 << " ^ " << n2 << " = " << n1 ^ n2 << endl;
//}

template <class T>
void Calculadora<T>::sqrt() {
	T n;
	n = pila_.pop();
	n = n.sqrt();
	pila_.push(n);
	cout << "sqrt(" << n << ") = " << n << endl;
}

template <class T>
Stack<T>& Calculadora<T>::get_stack() {
	return pila_;
}

#endif /* ENTITIES_CALCULADORA_H_ */
