/*
 * Real.cpp
 *
 *  Created on: Mar 9, 2016
 *      Author: sergio
 */

#include "Real.h"

Real::Real() {
    n_ = 0.;
}

Real::Real(double num) {
    n_ = num;
}

Real::Real(const Real &Real) {
    n_ = Real.n_;
}

Real::~Real() {}

double Real::getValue () const {
  return n_;
}

Real Real::operator+ (Real a) {
    return Real(n_ + a.n_);
}


Real Real::operator- (Real a) {
    return Real(n_ - a.n_);
}


Real Real::operator* (Real a) {
    return Real(n_ * a.n_);
}


Real Real::operator/ (Real a) {
    return Real(n_ / a.n_);
}

Real Real::operator^ (Real a) {
	return pow(n_, a.n_);
}

bool Real::operator== (Real a) {
    if(a.n_ == 0)
      throw ExcepcionDivisionPorCero();

    return (n_ == a.n_);
}

bool Real::operator!= (Real a) {
    return (n_ != a.n_);
}

bool Real::operator< (Real a) {
    return (n_ < a.n_);
}

bool Real::operator> (Real a) {
    return (n_ > a.n_);
}

bool Real::operator<= (Real a) {
    return (n_ <= a.n_);
}

bool Real::operator>= (Real a) {
    return (n_ >= a.n_);
}

Real Real::sqrt () {
  return std::sqrt(n_);
}

ostream& Real::toStream(ostream& sout) const {
    sout << n_;
    return sout;
}

istream& Real::fromStream(istream& sin) const {
    return sin;
}

ostream& operator << (ostream &os, const Real &Real) {
    os << Real.n_;
    return os;
}

istream& operator >> (istream &is, Real& real) {
    is >> real.n_;
    return is;
}
