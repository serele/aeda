/*
 * Natural.h
 *
 *  Created on: Mar 9, 2016
 *      Author: sergio
 */

#ifndef NATURAL_H_
#define NATURAL_H_

#include "../entities/Numero.h"
#include "Entero.h"

class Entero;

class Natural: public Numero {

private:
    unsigned int n_;

public:
    Natural();
    Natural(unsigned int);
    Natural(const Natural&);
    Natural(const Entero&);
    virtual ~Natural();

    unsigned int getValue () const;

    //prefijo, incrementa/decrementa y después establece el valor
    const Natural& operator++ ();
    const Natural& operator-- ();

    //postfijo, devuelve el valor que había antes de que fuera incre/decrementado
    const Natural operator++ (int);
    const Natural operator-- (int);

    Natural operator+ (Natural);
    Natural operator- (Natural);
    Natural operator* (Natural);
    Natural operator/ (Natural);
    Natural operator% (Natural);
    Natural operator^ (Natural);

    Natural& operator+= (Natural);
    Natural& operator-= (Natural);
    Natural& operator*= (Natural);
    Natural& operator/= (Natural);
    Natural& operator%= (Natural);

    bool operator== (Natural);
    bool operator!= (Natural);
    bool operator< (Natural);
    bool operator> (Natural);
    bool operator<= (Natural);
    bool operator>= (Natural);

    // Escribe un Numero al flujo sout
    ostream& toStream(ostream& sout) const;
    // Lee un Numero desde flujo sin
    istream& fromStream(istream& sin) const;

    friend ostream& operator << (std::ostream&, const Natural&);
    friend istream& operator >> (istream &i, Natural &natural);
};

#endif /* NATURAL_H_ */
