/*
 * Natural.cpp
 *
 *  Created on: Mar 9, 2016
 *      Author: sergio
 */

#include "Natural.h"

Natural::Natural() {
    n_ = 0;
}

Natural::Natural(unsigned int num) {
    if (num < 0)
      throw ExcepcionValorFueraDeRango();

    n_ = num;
}

Natural::Natural(const Natural &natural) {
    n_ = natural.n_;
}

Natural::Natural(const Entero &entero) {
    n_ = entero.getValue();
}

Natural::~Natural() {}

unsigned int Natural::getValue () const  {
  return n_;
}

const Natural& Natural::operator++ () { //prefijo
    ++n_;
    return *this;
}

const Natural& Natural::operator-- () { //prefijo
    --n_;
    return *this;
}

const Natural Natural::operator++ (int) { //postfijo
    Natural aux(*this);
    ++n_;
    return aux;
}

const Natural Natural::operator-- (int) { //postfijo
    Natural aux(*this);
    --n_;
    return aux;
}

Natural Natural::operator+ (Natural a) {
    return (Natural)(n_ + a.n_);
}


Natural Natural::operator- (Natural a) {
    if (n_ < a.n_)//comprueba que la resta no sea negativa
      throw ExcepcionValorFueraDeRango();
    return (Natural)(n_ - a.n_);
}


Natural Natural::operator* (Natural a) {
    return (Natural)(n_ * a.n_);
}


Natural Natural::operator/ (Natural a) {
    if(a.n_ == 0)
      throw ExcepcionDivisionPorCero();
    return (Natural)(n_ / a.n_);
}

Natural Natural::operator% (Natural a) {

    return (Natural)(n_ % a.n_);
}

Natural Natural::operator^ (Natural a) {
	return pow(n_, a.n_);
}

Natural& Natural::operator+= (Natural a) {
    n_ += a.n_;
    return *this;
}

Natural& Natural::operator-= (Natural a) {
    n_ -= a.n_;
    return *this;
}

Natural& Natural::operator*= (Natural a) {
    n_ *= a.n_;
    return *this;
}

Natural& Natural::operator/= (Natural a) {
    n_ /= a.n_;
    return *this;
}

Natural& Natural::operator%= (Natural a) {
    n_ %= a.n_;
    return *this;
}

bool Natural::operator== (Natural a) {
    return(n_ == a.n_);
}

bool Natural::operator!= (Natural a) {
    return(n_ != a.n_);
}

bool Natural::operator< (Natural a) {
    return (n_ < a.n_);
}

bool Natural::operator> (Natural a) {
    return (n_ > a.n_);
}

bool Natural::operator<= (Natural a) {
    return (n_ <= a.n_);
}

bool Natural::operator>= (Natural a) {
    return (n_ >= a.n_);
}

ostream& Natural::toStream(ostream& sout) const {
    sout << n_;
    return sout;
}

istream& Natural::fromStream(istream& sin) {
    return sin;
}

ostream& operator << (ostream &o, const Natural &natural)  {
    o << natural.n_;
    return o;
}

istream& operator >> (istream &i, Natural &natural) {
	i >> natural.n_;
	return i;
}
