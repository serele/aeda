/*
 * Entero.h
 *
 *  Created on: Mar 9, 2016
 *      Author: sergio
 */

#ifndef ENTERO_H_
#define ENTERO_H_

#include "../entities/Numero.h"
#include "Natural.h"
#include "Real.h"

class Natural;

class Entero : public Numero {

private:
    int n_;

public:
    Entero();
    Entero(int);
    Entero(const Entero&);
    Entero(const Natural&);
    Entero(const Real&);
    virtual ~Entero();

    int getValue () const;

    Entero operator+ (Entero);
    Entero operator- (Entero);
    Entero operator* (Entero);
    Entero operator/ (Entero);
    Entero operator% (Entero);

    Entero operator^ (Entero);
    Real sqrt();
//    Entero sqrt();

    bool operator== (Entero);
    bool operator!= (Entero);
    bool operator< (Entero);
    bool operator> (Entero);
    bool operator<= (Entero);
    bool operator>= (Entero);

    // Escribe un Numero al flujo sout
    ostream& toStream(ostream& sout) const;
    // Lee un Numero desde flujo sin
    istream& fromStream(istream& sin);


    friend ostream& operator << (ostream&, const Entero&);

    friend istream& operator >> (istream&, Entero&);
};

#endif /* ENTERO_H_ */
