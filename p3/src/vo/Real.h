/*
 * Real.h
 *
 *  Created on: Mar 9, 2016
 *      Author: sergio
 */

#ifndef REAL_H_
#define REAL_H_

#include "../entities/Numero.h"

class Real: public Numero {
private:
    double n_;

public:
    Real();
    Real(double);
    Real(const Real&);
    virtual ~Real();

    double getValue () const;

    Real operator+ (Real);
    Real operator- (Real);
    Real operator* (Real);
    Real operator/ (Real);
    Real operator^ (Real);

    bool operator== (Real);
    bool operator!= (Real);
    bool operator< (Real);
    bool operator> (Real);
    bool operator<= (Real);
    bool operator>= (Real);

    Real sqrt ();

    // Escribe un Numero al flujo sout
    ostream& toStream(ostream& sout) const;
    // Lee un Numero desde flujo sin
    istream& fromStream(istream& sin);

    friend ostream& operator << (ostream&, const Real&);
    friend istream& operator >> (istream&, Real&);
};

#endif /* REAL_H_ */
