/*
 * Complejo.cpp
 *
 *  Created on: Mar 9, 2016
 *      Author: sergio
 */

#include "Complejo.h"

Complejo::Complejo() {
    real_ = 0.;
    imaginario_ = 0.;
}

Complejo::Complejo(Real r, Real i) {
    real_ = r;
    imaginario_ = i;
}

Complejo::Complejo(const Complejo& c) {
    real_ = c.real_;
    imaginario_ = c.imaginario_;
}

Complejo::~Complejo() {}

Real Complejo::modulo() {
  Real r = real_*real_ + imaginario_*imaginario_;
  return r.sqrt();
}

Complejo Complejo::conjugado() {
  return Complejo(this->real_, this->imaginario_ * (-1));
}

Complejo Complejo::operator+ (Complejo c) {
    Complejo aux;
    aux.real_ = real_ + c.real_;
    aux.imaginario_ = imaginario_ + c.imaginario_;

    return aux;

}

Complejo Complejo::operator- (Complejo c) {
    Complejo aux;
    aux.real_ = real_ - c.real_;
    aux.imaginario_ = imaginario_ - c.imaginario_;

    return aux;
}

Complejo Complejo::operator* (Complejo c) {
    Complejo aux1, aux2;

    aux1.real_ = real_ * c.real_;
    aux1.imaginario_ = real_ * c.imaginario_;

    aux2.imaginario_ = imaginario_ * c.real_;
    aux2.real_ = imaginario_ * c.imaginario_ * (-1); //i^2 se cambia de signo

    aux1.real_ = aux1.real_ + aux2.real_;
    aux1.imaginario_ = aux1.imaginario_ + aux2.imaginario_;

    return aux1;
}

Complejo Complejo::operator/ (Complejo c) {
    Complejo aux1, aux2;

    c = c.conjugado();

    aux1 = *this * c;//numerador
    aux2 = c * c.conjugado(); //denominador



    aux1.real_ = aux1.real_ / aux2.real_;
    aux1.imaginario_ = aux1.imaginario_ / aux2.real_;

    return aux1;
}

bool Complejo::operator== (Complejo c){
  return (this->modulo() == c.modulo());
}


bool Complejo::operator!= (Complejo c) {
  return (this->modulo() != c.modulo());
}

bool Complejo::operator< (Complejo c) {
  return (this->modulo() < c.modulo());
}

bool Complejo::operator> (Complejo c) {
  return (this->modulo() > c.modulo());
}

bool Complejo::operator<= (Complejo c) {

    return (this->modulo() <= c.modulo());
}

bool Complejo::operator>= (Complejo c) {
  return (this->modulo() >= c.modulo());
}

ostream& Complejo::toStream(ostream& sout) const {
    sout << "(" <<  real_ << ((Real)imaginario_ > 0 ? "+" : "") << imaginario_ << "i)" ;

    return sout;
}

istream& Complejo::fromStream(istream& sin) {
    return sin;
}

ostream& operator << (ostream &o, const Complejo &c) {
    o << "(" <<  c.real_ << ((Real)c.imaginario_ > 0 ? "+" : "") << c.imaginario_ << "i)" ;

    return o;
}

istream& operator >> (istream &is, Complejo &complejo) {
    is >> complejo.real_;
    is >> complejo.imaginario_;
    return is;
}
