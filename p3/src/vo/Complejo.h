/*
 * Complejo.h
 *
 *  Created on: Mar 9, 2016
 *      Author: sergio
 */

#ifndef COMPLEJO_H_
#define COMPLEJO_H_

#include "../entities/Numero.h"
#include "Real.h"

using namespace std;

class Complejo : public Numero {

private:
    Real real_;
    Real imaginario_;


public:
    Complejo();
    Complejo(Real, Real);
    Complejo(const Complejo&);
    virtual ~Complejo();

    Real modulo ();
    Complejo conjugado ();

    Complejo operator+ (Complejo);
    Complejo operator- (Complejo);
    Complejo operator* (Complejo);
    Complejo operator/ (Complejo);
    Complejo sqrt() {}

    bool operator== (Complejo);
    bool operator!= (Complejo);
    bool operator< (Complejo);
    bool operator> (Complejo);
    bool operator<= (Complejo);
    bool operator>= (Complejo);

    // Escribe un Numero al flujo sout
    ostream& toStream(ostream& sout) const;
    // Lee un Numero desde flujo sin
    istream& fromStream(istream& sin);

    friend ostream& operator << (ostream&, const Complejo&);
    friend istream& operator >> (istream&, Complejo&);
};
#endif /* COMPLEJO_H_ */
