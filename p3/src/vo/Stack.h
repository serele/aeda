/*
 * Stack.h
 *
 *  Created on: Mar 8, 2016
 *      Author: sergio
 */

#ifndef STACK_H_
#define STACK_H_

#include "../entities/List.h"

template <class T>

class Stack {
private:
  List<T> *stack_;


public:
  Stack();
  Stack(const Stack&);
  ~Stack();

  void push(T);
  T pop();
  List<T>& get_stack();


  friend ostream& operator << (std::ostream& o, const Stack<T>& s) {
    return o << *s.stack_;
  }

};

template <class T>
Stack<T>::Stack() {
  stack_ = new List<T>();
}

template <class T>
Stack<T>::Stack(const Stack& s) {
    stack_ = s.stack_;
}

template <class T>
Stack<T>::~Stack() {
}

template <class T>
void Stack<T>::push(T n) {
  stack_->push_front(n);
}

template <class T>
T Stack<T>::pop() {
  return stack_->pop();
}

template <class T>
List<T>& Stack<T>::get_stack() {
	return stack_;
}

#endif /* STACK_H_ */
