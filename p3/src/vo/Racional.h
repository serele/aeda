/*
 * Racional.h
 *
 *  Created on: Mar 9, 2016
 *      Author: sergio
 */

#ifndef RACIONAL_H_
#define RACIONAL_H_

#include "../entities/Numero.h"
#include "Entero.h"
#include "Natural.h"

class Racional : public Numero {

private:
    Entero n_;//numerador
    Natural d_;//denominador

public:
    Racional();
    Racional(Entero, Natural);
    Racional(const Racional&);
    virtual ~Racional();

    Entero maximoComunDivisor(Entero, Entero);
    void simplificar();

    Racional operator+ (Racional);
    Racional operator- (Racional);
    Racional operator* (Racional);
    Racional operator/ (Racional);
    Racional sqrt () {}

    bool operator== (Racional);
    bool operator!= (Racional);
    bool operator< (Racional);
    bool operator> (Racional);
    bool operator<= (Racional);
    bool operator>= (Racional);

    // Escribe un Numero al flujo sout
    ostream& toStream(ostream& sout) const;
    // Lee un Numero desde flujo sin
    istream& fromStream(istream& sin);

    friend ostream& operator << (std::ostream&, const Racional&);
    friend istream& operator >> (istream&, Racional&);
};
#endif /* RACIONAL_H_ */
