/*
 * Racional.cpp
 *
 *  Created on: Mar 9, 2016
 *      Author: sergio
 */

#include "Racional.h"

Racional::Racional() {
    n_ = 0;
    d_ = 1;
}

Racional::Racional(Entero num, Natural denom) {
    n_ = num;
    d_ = denom;
  if (denom == 0) {
      throw ExcepcionDivisionPorCero();
  }
  simplificar();
}

Racional::Racional(const Racional &racional) {
    n_ = racional.n_;
    d_ = racional.d_;
}

Racional::~Racional() {}

Entero Racional::maximoComunDivisor(Entero _n, Entero resto) {
    if (resto == 0)
        return _n;
    else
      return maximoComunDivisor(resto, _n % resto);

}

void Racional::simplificar () {

    Entero rdc = 0;
    if(d_ > n_)
        rdc = maximoComunDivisor(d_, n_);
    else
        rdc = maximoComunDivisor(n_, d_);

    if (rdc < 0) {
      n_ = (n_ / (rdc * (-1)));
      d_ = (Natural)(d_ / (rdc * (-1)));
    }
    else {
      n_ = (n_ / rdc);
      d_ = (Natural)(d_ / rdc);
    }
}


Racional Racional::operator+ (Racional r) {

    Entero num = (n_ * r.d_) + (d_ * r.n_);
    Natural den = d_ * r.d_;

    Racional aux(num, den);

    return aux;
}


Racional Racional::operator- (Racional r) {

    Entero num = (n_ * r.d_) - (d_ * r.n_);
    Natural den = d_ * r.d_;

    Racional aux(num, den);

    return aux;
}


Racional Racional::operator* (Racional r) {

    Entero num = r.n_ * n_;
    Natural den = r.d_ * d_;

    Racional aux(num, den);
    return aux;
}


Racional Racional::operator/ (Racional r) {

    Entero num = n_ * r.d_;
    Natural den = d_ * r.n_;
    Racional aux;
    aux = Racional(num, den);

    return aux;
}


bool Racional::operator== (Racional r) {
    return (r.n_ == n_ && r.d_ == d_);
}

bool Racional::operator!= (Racional r) {
    return (r.n_ != n_ || r.d_ != d_);
}

bool Racional::operator< (Racional r) {
    return ((n_ * (Entero)r.d_ - (Entero)d_ * r.n_) < 0);
}

bool Racional::operator> (Racional r) {
    return ((n_ * (Entero)r.d_ - (Entero)d_ * r.n_) > 0);
}

bool Racional::operator<= (Racional r) {
    return ((n_ * (Entero)r.d_ - (Entero)d_ * r.n_) <= 0);
}

bool Racional::operator>= (Racional r) {
    return ((n_ * (Entero)r.d_ - (Entero)d_ * r.n_) >= 0);
}

ostream& Racional::toStream(ostream& sout) const {
    sout << n_;
    return sout;
}

istream& Racional::fromStream(istream& sin) {
    return sin;
}

ostream& operator << (ostream &o, const Racional &racional) {
    o << "(" << racional.n_ << "/" << racional.d_ << ")";
    return o;
}

istream& operator >> (istream &is, Racional &racional) {
//	char bar;
    is >> racional.n_;
//    is >> bar;
	is >> racional.d_;
    return is;
}
