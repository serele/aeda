/*
 * Entero.cpp
 *
 *  Created on: Mar 9, 2016
 *      Author: sergio
 */

#include "Entero.h"

Entero::Entero() {
    n_ = 0;
}

Entero::Entero(int num) {
    n_ = num;
}

Entero::Entero(const Entero &a) {
    n_ = a.n_;
}

//entero desde natural
Entero::Entero(const Natural &a) {
    n_ = a.getValue();
}

//entero desde real
Entero::Entero(const Real &a) {
    n_ = a.getValue();
}

Entero::~Entero() {}

int Entero::getValue () const{
  return n_;
}

Entero Entero::operator+ (Entero a) {
    return n_ + a.n_;
}


Entero Entero::operator- (Entero a) {
    return n_ - a.n_;
}


Entero Entero::operator* (Entero a) {

    return n_ * a.n_;
}


Entero Entero::operator/ (Entero a) {
    if(a.n_ == 0)
      throw ExcepcionDivisionPorCero();

    return n_ / a.n_;
}

Entero Entero::operator% (Entero a) {

    return n_ % a.n_;
}

Entero Entero::operator^ (Entero a) {
	return pow(n_, a.n_);
}

Real Entero::sqrt() {
	Real n = n_;
	return n.sqrt();
}

//Entero Entero::sqrt() {
//	return n_^(1/2);
//}

bool Entero::operator== (Entero a) {
    return (n_ == a.n_);
}

bool Entero::operator!= (Entero a) {
    return (n_ != a.n_);
}

bool Entero::operator< (Entero a) {
    return (n_ < a.n_);
}

bool Entero::operator> (Entero a) {
    return (n_ > a.n_);
}

bool Entero::operator<= (Entero a) {
    return(n_ <= a.n_);
}

bool Entero::operator>= (Entero a) {
    return(n_ >= a.n_);
}

ostream& Entero::toStream(ostream& sout) const {
    sout << n_;
    return sout;
}

istream& Entero::fromStream(istream& sin) {
    return sin;
}

ostream& operator << (ostream &o, const Entero &entero) {
    o << entero.n_;
    return o;
}

istream& operator >> (istream &i, Entero &entero) {
	i >> entero.n_;
	return i;
}
