/*
 * Numero.h
 *
 *  Created on: Mar 9, 2016
 *      Author: sergio
 */

#ifndef NUMERO_H_
#define NUMERO_H_

#include <cmath>
//#include "../vo/Entero.h"
//#include "../vo/Racional.h"
//#include "../vo/Real.h"
//#include "../vo/Complejo.h"
#include "Excepciones.h"

using namespace std;

class Numero {

    public:
//	 	// Devuelve una copia del Numero actual en el tipo Entero
//	    virtual const Entero toEntero() const = 0;
//	    // Devuelve una copia del Numero actual en el tipo Racional
//	    virtual const Racional toRacional() const = 0;
//	    // Devuelve una copia del Numero actual en el tipo Real
//	    virtual const Real toReal() const = 0;
//	    // Devuelve una copia del Numero actual en el tipo Complejo
//	    virtual const Complejo toComplejo();
	    // Escribe un Numero al flujo sout
        virtual ostream& toStream (ostream& sout) const = 0;
	    // Lee un Numero desde flujo sin
        virtual istream& fromStream (istream& sin) = 0;
};



#endif /* NUMERO_H_ */
