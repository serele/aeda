/*
 * Excepciones.h
 *
 *  Created on: Mar 9, 2016
 *      Author: sergio
 */

#ifndef EXCEPCIONES_H_
#define EXCEPCIONES_H_


#include <iostream>
#include <cstdlib>
#include <exception>

using namespace std;



class ExcepcionDivisionPorCero : public exception {
public:
    const char* what() const throw() {
	return "Error: división por cero...";
    }
};


class ExcepcionValorFueraDeRango : public exception {
public:
    const char* what() const throw() {
	return "Error: valor fuera de rango...";
    }
};


#endif /* EXCEPCIONES_H_ */
