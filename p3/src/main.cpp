//============================================================================
// Name        : Algoritmos y Estructuras de Datos Avanzadas P3
// Author      : Sergio Reyes de León
// Version     :
// Copyright   : Your copyright notice
// Description : Práctica 3: Polimorfismo y excepciones
//============================================================================

#include <iostream>
#include <sstream>
#include <string.h>
#include <string>
#include <cctype>

#include "vo/Stack.h"
#include "vo/Entero.h"
#include "vo/Natural.h"
#include "vo/Real.h"
#include "vo/Complejo.h"
#include "vo/Racional.h"

#include "entities/Calculadora.h"


using namespace std;

int main() {

	Calculadora<Entero> calc;

	system("clear");

	cout << "Expresión a calcular en notación postfija (polaca inversa): ";
	cout.flush();

	char str;

	while (cin.peek() != '\n') {
		cin >> ws;
		char c = cin.peek();
		if ( isdigit(c) ) { //si es un número
///
//			c = cin.peek();
//			while (cin.peek() != ' ') {
//
//			}
//			switch(c) {
//			case ''
//			}
///
			Entero n;
			cin >> n;
			calc.push(n);
			cout << "Operando: " << n << endl;
			cout << "Contenido pila: " << calc.get_stack();
		}
		else {
			char str;
			cin >> str;
			if(str == '-' && isdigit(cin.peek())) { //número negativo
				Entero n;
				cin >> n;
				calc.push(n);
				cout << "Operando: -" << n << endl;
				cout << "Contenido pila: " << calc.get_stack();
			}
			else {
				cout << "Operador: " << str << endl;
				switch ( str ) {
					case '+':
						calc.suma();
						break;
					case '-':
						calc.resta();
						break;
					case '*':
						calc.multiplicacion();
						break;
					case '/':
						calc.division();
						break;
//					case '^':
//						calc.potencia();
//						break;
					case 'r'://raíz cuadrada
						calc.sqrt();
						break;
				}//end switch
			}//end else
		}//end if
	}//end while

	return 0;
}
